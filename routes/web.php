<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/logs/body',[Controllers\Logs::class, 'body'])->middleware(['auth']);
Route::get('/logs',[Controllers\Logs::class, 'page'])->middleware(['auth'])->name('logs');

Route::post('/dashboard/remove',[Controllers\Panel::class, 'remove'])->middleware(['auth']);
Route::post('/dashboard/save',[Controllers\Panel::class, 'save'])->middleware(['auth']);
Route::get('/dashboard/edit/{id}',[Controllers\Panel::class, 'edit'])->middleware(['auth']);
Route::get('/dashboard',[Controllers\Panel::class, 'page'])->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
