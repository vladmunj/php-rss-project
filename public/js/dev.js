function ajax($url,$data,callback=function(){},failed=function(){},$type="POST"){
    let $csrf = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: $url,
        data: $data,
        type: $type,
        headers: {
            'X-CSRF-Token' : $csrf
        },
        success: function(response){
            callback(response);
        },
        error: function(response){
          failed(response);
        }
    });
}

$(function(){
	$(document).on('click','.show-log-body',function(){
		let $el = $(this);
		let $id = $el.data('id');
		let $bodyBlock = $('.log-body-block');
		$bodyBlock.addClass('hidden');

		ajax('/logs/body',{id:$id},function(response){
			$bodyBlock.find('.log-body-raw').html(response);
			$bodyBlock.removeClass('hidden');
		});
		return false;
	});

	$(document).on('click','.hide-log-body',function(){
		let $el = $(this);
		let $block = $el.parents('.log-body-block');
		$block.addClass('hidden');
		return false;
	});

	$(document).on('click','.article-edit',function(){
		let $el = $(this);
		let $id = $el.data('id');

		window.location.href = '/dashboard/edit/'+$id;
		return false;
	});

	$(document).on('click','.article-remove',function(){
		let $el = $(this);
		let $id = $el.data('id');

		ajax('/dashboard/remove',{id:$id},function(response){
			location.reload();
		});
		return false;
	});
});