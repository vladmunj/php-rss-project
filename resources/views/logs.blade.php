<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Логи
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 hidden log-body-block">
                    <a href="#" class="hide-log-body">x</a>
                    <div class="log-body-raw"></div>
                </div>
            </div>
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="w-full">
                        <thead>
                            <tr>
                                <th>Дата и время</th>
                                <th>Request Method</th>
                                <th>Request URL</th>
                                <th>Response HTTP Code</th>
                                <th>Response Body</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($logs as $log)
                                <tr>
                                    <td>{{ $log->created_at }}</td>
                                    <td>{{ $log->method }}</td>
                                    <td>{{ $log->url }}</td>
                                    <td>{{ $log->code }}</td>
                                    <td><a href="#" class="show-log-body" data-id="{{ $log->id }}">Нажмите для просмотра тела запроса</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $logs->onEachSide(5)->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
