<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Новости
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="w-full">
                        <thead>
                            <tr>
                                <th>Название</th>
                                <th>Ссылка</th>
                                <th>Краткое описание</th>
                                <th>Дата и время публикации</th>
                                <th>Автор</th>
                                <th>Медиа</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($articles as $item)
                                <tr>
                                    <td>
                                        {{ $item->name }}
                                    </td>
                                    <td>
                                        <a href="{{ $item->link }}" target="_blank">{{ $item->link }}</a>
                                    </td>
                                    <td>{{ $item->short }}</td>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->author }}</td>
                                    <td>
                                        @isset($item->images)
                                            @foreach($item->images as $link)
                                                <a href="{{ $link }}" target="_blank">{{ $link }}</a>
                                            @endforeach
                                        @endisset
                                        <div class="actions">
                                            <svg class="h-4 w-4 text-blue-500 article-edit" data-id="{{ $item->id }}" <svg  width="24"  height="24"  viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round">  <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7" />  <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z" /></svg>
                                            <svg class="h-4 w-4 text-blue-500 article-remove" data-id="{{ $item->id }}"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round">  <path d="M21 4H8l-7 8 7 8h13a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z" />  <line x1="18" y1="9" x2="12" y2="15" />  <line x1="12" y1="9" x2="18" y2="15" /></svg>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $articles->onEachSide(5)->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
