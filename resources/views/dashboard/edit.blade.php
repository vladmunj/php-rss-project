<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Редактировать новость
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- component -->
            <div class="relative min-h-screen flex items-center justify-center bg-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8 bg-gray-500 bg-no-repeat bg-cover relative items-center"
            style="background-image: url(https://images.unsplash.com/photo-1532423622396-10a3f979251a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1500&q=80);">
                <div class="absolute bg-black opacity-60 inset-0 z-0"></div>
                <div class="max-w-md w-full space-y-8 p-10 bg-white rounded-xl shadow-lg z-10">
                        <div class="grid  gap-8 grid-cols-1">
                            <div class="flex flex-col ">
                                    <div class="flex flex-col sm:flex-row items-center">
                                        <h2 class="font-semibold text-lg mr-auto">Новость #{{ $article->id }}</h2>
                                        <div class="w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0"></div>
                                    </div>
                                    <div class="mt-5">
                                        <form method="POST" action="/dashboard/save" class="form">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $article->id }}">
                                            <div class="mb-3 space-y-2 w-full text-xs">
                                                <label class=" font-semibold text-gray-600 py-2">Название</label>
                                                <div class="flex flex-wrap items-stretch w-full mb-4 relative">
                                                    <div class="flex">
                                                        <span class="flex items-center leading-normal bg-grey-lighter border-1 rounded-r-none border border-r-0 border-blue-300 px-3 whitespace-no-wrap text-grey-dark text-sm w-12 h-10 bg-blue-300 justify-center items-center  text-xl rounded-lg text-white">
                                                        <svg class="h-6 w-6 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <circle cx="18" cy="16" r="3" />  <line x1="21" y1="13" x2="21" y2="19" />  <path d="M3 19l5 -13l5 13" />  <line x1="5" y1="14" x2="11" y2="14" /></svg>
                                                       </span>
                                                    </div>
                                                    <input type="text" class="flex-shrink flex-grow flex-auto leading-normal w-px flex-1 border border-l-0 h-10 border-grey-light rounded-lg rounded-l-none px-3 relative focus:border-blue focus:shadow" name="name" value="{{ $article->name }}">
                                                </div>
                                            </div>
                                            <div class="mb-3 space-y-2 w-full text-xs">
                                                <label class=" font-semibold text-gray-600 py-2">Ссылка</label>
                                                <div class="flex flex-wrap items-stretch w-full mb-4 relative">
                                                    <div class="flex">
                                                        <span class="flex items-center leading-normal bg-grey-lighter border-1 rounded-r-none border border-r-0 border-blue-300 px-3 whitespace-no-wrap text-grey-dark text-sm w-12 h-10 bg-blue-300 justify-center items-center  text-xl rounded-lg text-white">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                        </svg>
                                                       </span>
                                                    </div>
                                                    <input type="text" class="flex-shrink flex-grow flex-auto leading-normal w-px flex-1 border border-l-0 h-10 border-grey-light rounded-lg rounded-l-none px-3 relative focus:border-blue focus:shadow" name="link" value="{{ $article->link }}">
                                                </div>
                                            </div>
                                            <div class="mb-3 space-y-2 w-full text-xs">
                                                <label class=" font-semibold text-gray-600 py-2">Дата</label>
                                                <div class="flex flex-wrap items-stretch w-full mb-4 relative">
                                                    <div class="flex">
                                                        <span class="flex items-center leading-normal bg-grey-lighter border-1 rounded-r-none border border-r-0 border-blue-300 px-3 whitespace-no-wrap text-grey-dark text-sm w-12 h-10 bg-blue-300 justify-center items-center  text-xl rounded-lg text-white">
                                                        <svg class="h-6 w-6 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <rect x="4" y="5" width="16" height="16" rx="2" />  <line x1="16" y1="3" x2="16" y2="7" />  <line x1="8" y1="3" x2="8" y2="7" />  <line x1="4" y1="11" x2="20" y2="11" />  <line x1="11" y1="15" x2="12" y2="15" />  <line x1="12" y1="15" x2="12" y2="18" /></svg>
                                                       </span>
                                                    </div>
                                                    <input type="text" class="flex-shrink flex-grow flex-auto leading-normal w-px flex-1 border border-l-0 h-10 border-grey-light rounded-lg rounded-l-none px-3 relative focus:border-blue focus:shadow" name="date" value="{{ $article->date }}">
                                                </div>
                                            </div>
                                            <div class="mb-3 space-y-2 w-full text-xs">
                                                <label class=" font-semibold text-gray-600 py-2">Автор</label>
                                                <div class="flex flex-wrap items-stretch w-full mb-4 relative">
                                                    <div class="flex">
                                                        <span class="flex items-center leading-normal bg-grey-lighter border-1 rounded-r-none border border-r-0 border-blue-300 px-3 whitespace-no-wrap text-grey-dark text-sm w-12 h-10 bg-blue-300 justify-center items-center  text-xl rounded-lg text-white">
                                                        <svg class="h-6 w-6 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <circle cx="12" cy="12" r="9" />  <path d="M14.5 9a3.5 4 0 1 0 0 6" /></svg>
                                                       </span>
                                                    </div>
                                                    <input type="text" class="flex-shrink flex-grow flex-auto leading-normal w-px flex-1 border border-l-0 h-10 border-grey-light rounded-lg rounded-l-none px-3 relative focus:border-blue focus:shadow" name="author" value="{{ $article->author }}">
                                                </div>
                                            </div>
                                            <div class="flex-auto w-full mb-1 text-xs space-y-2">
                                                <label class="font-semibold text-gray-600 py-2">Краткое описание</label>
                                                <textarea required="" name="short" class="w-full min-h-[100px] max-h-[300px] h-28 appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg  py-4 px-4"  spellcheck="false">{{ $article->short }}</textarea>
                                            </div>

                                            <div class="mt-5 text-right md:space-x-3 md:block flex flex-col-reverse">
                                                <a href="/dashboard" class="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100"> Вернуться </a>
                                                <button type="submit" class="mb-2 md:mb-0 bg-green-400 px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-green-500">Сохранить</button>
                                            </div>
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>