<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Article;

class Panel extends BaseController
{
    public function page(){
        $articles = Article::paginate(15);
        foreach($articles as $article){
            $article->images = json_decode($article->image);
        }

        return view('dashboard',['articles'=>$articles]);
    }

    public function edit(Request $request){
        $article = Article::find($request->id);
        return view('dashboard.edit',['article'=>$article]);
    }

    public function save(Request $request){
        $fields = [
            'name' => $request->name,
            'link' => $request->link,
            'short' => $request->short,
            'date' => $request->date,
            'author' => $request->author
        ];

        Article::where('id',$request->id)->update($fields);
        
        return redirect('/dashboard/edit/'.$request->id);
    }

    public function remove(Request $request){
        $article = Article::find($request->id);
        $article->delete();
    }
}
