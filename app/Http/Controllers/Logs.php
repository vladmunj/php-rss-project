<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Log;

class Logs extends BaseController
{
    public function page(){
        $logs = Log::paginate(10);
        return view('logs',['logs'=>$logs]);
    }

    public function body(Request $request){
        $log = Log::find($request->id);
        return $log->body;
    }
}
