<?php namespace App\Console\Helpers;

use Illuminate\Support\Facades\Http;
use App\Models\Log;
use Bmatovu\LaravelXml\Support\Facades\LaravelXml;

class WebClient{
	private $response;

	public function __construct(){
		$this->response = Http::get(env('RSS_URL'));
	}

	public function check(){
		if($this->response->status() != 200){
			$this->log();
			return false;
		}

		return true;
	}

	public function log(){
		$logModel = new Log;
		$logModel->method = "GET";
		$logModel->url = env('RSS_URL');
		$logModel->code = $this->response->status();
		$logModel->body = $this->response->body();

		$logModel->save();
	}

	public function crawler(){
		$parser = new LaravelXml();

		$data = $parser->decode($this->response->body());
		return $data['channel']['item'];
	}
}

?>