<?php namespace App\Console\Helpers;

use App\Models\Article;

class Articles{
	public function clear(){
		Article::truncate();
	}

	public function upload($data){
		foreach($data as $item){
			$this->save($item);
		}
	}

	private function save($info){
		$fields = [
			'name' => $info['title'],
			'link' => $info['link'],
			'short' => $info['description']
		];

		if(Arr::check($info,'author')) $fields['author'] = $info['author'];
		if(Arr::check($info,'pubDate')){
			$formattedDate = date('Y-m-d H:i:s',strtotime($info['pubDate']));
			$fields['date'] = $formattedDate;
		}
		if(Arr::check($info,'enclosure')){
			$images = [];
			foreach($info['enclosure'] as $image){
				if(Arr::check($image,'@attributes') && Arr::check($image['@attributes'],'url')){
					$images[] = $image['@attributes']['url'];
				}
			}

			$fields['image'] = json_encode($images);
		}

		$article = Article::create($fields);
		dump('Новость "'.$fields['name'].'" добавлена');
	}
}

?>