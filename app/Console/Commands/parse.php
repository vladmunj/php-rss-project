<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Helpers\WebCLient;
use App\Console\Helpers\Articles;

class parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запуск парсера rss - ленты';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Инициализация подключения к RSS - ленте...');
        $this->info('Url ленты: '.env('RSS_URL'));
        $webC = new WebCLient();

        if(!$webC->check()){
            $this->error('Ошибка соединения. Подробную информацию Вы сможете найти в логах');
            return false;
        }

        $this->info('Логирование запроса...');
        $webC->log();

        $this->info('Парсинг...');
        $data = $webC->crawler();

        $this->info('Удаление текущей базы новостей...');
        $articlesC = new Articles();
        $articlesC->clear();

        $this->info('Сохранение новостей в базу данных...');
        $articlesC->upload($data);

        $this->comment('Загрузка завершена');
    }
}
